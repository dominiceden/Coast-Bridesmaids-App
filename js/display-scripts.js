$(document).ready(function(){

// add FastClick.js to document to remove 300ms touch lag on touch devices

  $(function() {
      FastClick.attach(document.body);
  });

// When the document is ready, populate the bottom thumbnails section, main hero section and right hand side images

  for (var i = 0; i < products.dresses.length; i++) {
    thumbnailUrl = products.dresses[i].bottomthumbnail; // store the URL for the dress bottom thumbnail image in this variable
    // Now create a div to hold this thumbnail, give it the same ID as the ID of the product and write it to the DOM.
    $("#bottom-thumbnail-container").append('<div class="bottom-thumbnail" id="' + products.dresses[i].id + '"><img src="' + thumbnailUrl + '"></div>');
  };

// When a bottom thumbnail is clicked on, run the following function

  $(".bottom-thumbnail").click(function(){

    $("#slider").remove(); // remove the slider if it's present, as most products do not have one (only 5/15 products do).

    var productId = parseInt( $(this).attr("id") -1); // get product id that was clicked on and store it in a variable called productId. Use -1 as array indices start at 0 and our product ids start at 1

    // create a set of variables to hold the attributes of the product that was clicked on
    var productSeparates = products.dresses[productId].separates; // to check if the product is made up of separate items or not (boolean).
    var productName = products.dresses[productId].name;
    var productGBPPrice = products.dresses[productId].GBPPrice;
    var productEURPrice = products.dresses[productId].EURPrice;
    var productSize = products.dresses[productId].size;
    var productColours = products.dresses[productId].colours;
    var productLongColourName = products.dresses[productId].longcolourname; // if the colour name is long, it gets a wider div.
    var productPetite = products.dresses[productId].petite;
    var productIsAvailable = products.dresses[productId].isAvailable;
    var productIn5Or15 = products.dresses[productId].in5or15;
    var productIn5Or15Images = products.dresses[productId].in5or15images; // list of images that will be used in the 5 or 15 slider
    var productBottomText = products.dresses[productId].bottomtext;
    var productAlsoAvailable = products.dresses[productId].alsoavailable;

    // Get the URLs for the images for this particular product and store them in variables
    var productSmallImageFront = products.dresses[productId].smallimagefront;
    var productSmallImageBack = products.dresses[productId].smallimageback;
    var productSmallImageDetail = products.dresses[productId].smallimagedetail;
    var productLargeImageFront = products.dresses[productId].largeimagefront;
    var productLargeImageBack = products.dresses[productId].largeimageback;
    var productLargeImageDetail = products.dresses[productId].largeimagedetail;

    // Now write the product information to the DOM
    $("#productName").html(productName);

    if (countrycode == "UK") {
      $("#productPrice").html(productGBPPrice);
    }
    else if (countrycode == "IE") {
      $("#productPrice").html(productEURPrice);
    }

    $("#productSize").html(productSize);

    // Generate product colours

    $("#colours").html(""); // Clear any lingering colours first before adding new ones.

    for(var key in productColours) { // Loop through the colours and create divs with the key name and the value as a background.
    $("#colours").append('<div class="colour" style="display:inline-block"><div class="colour-circle" style="background:' + productColours[key] + '"></div><br><p class="colour-name">' + key + '</p></div>');
    };

    // Test if product is available in petite sizes. If it is, then write 'petite' to the DOM.
    if (productPetite == true) {
      $("#productPetite").html("petite");
    }
    else {
      $("#productPetite").html("");
    };

    // Add Shop Now button if the product is in stock, or add an Email Me When In Stock button
    if (productIsAvailable == true) {
      $("#productCTA").html("shop now");
    }

    else {
      $("#productCTA").html("email me when back in stock");
    };

// *** SEPARATES ***
    // If product is made up of separate items, create two sections to hold the product information
    if (productSeparates == true) {
      $("#right-div").remove(); // remove right div if it is present from previous separates, as we will be re-adding it later and it cannot clash
      $("#left-div").clone().appendTo("#bottom-slider").attr("id", "right-div"); // clone left div, call it right div and put it adjacent to left div
      $("#left-div, #right-div").css({"width": "50%", "display": "block", "float": "left"}); // set the two divs next to each other
      $("#left-div").css("box-shadow", "2px 0 2px -2px #d1d1d1");

      // create variables for the second product
      var productName2 = products.dresses[productId].name2;
      var productGBPPrice2 = products.dresses[productId].GBPPrice2;
      var productEURPrice2 = products.dresses[productId].EURPrice2;
      var productSize2 = products.dresses[productId].size2;
      var productColours2 = products.dresses[productId].colours2;
      var productPetite2 = products.dresses[productId].petite2;
      var productAlsoAvailable2 = products.dresses[productId].alsoavailable2;

      $("#right-div > #productName").attr("id", "productName2"); // change #productName to #productName2 so we can write correct name to it.
      $("#productName2").html(productName2); // write productName 2 to the DOM element

      $("#right-div #sizes").attr("id", "sizes2"); // change #sizes to #sizes2.

      $("#right-div #sizes span#productPrice").attr("id", "productPrice2"); // change #productPrice to #productPrice2 so we can write correct price to it.
      $("span#productPrice2").html(productGBPPrice2); // write productPrice2 to the DOM element

      $("#right-div span#productSize").attr("id", "productSize2"); // change #productSize to #productSize2 so we can write correct price to it.
      $("span#productSize2").html(productSize2); // write productSize2 to the DOM element

      $("#right-div #colours").attr("id", "colours2");
      $("#colours2").html(""); // Clear any lingering colours first before adding new ones.

      for(var key in productColours2) { // Loop through the colours and create divs with the key name and the value as a background.
      $("#colours2").append('<div class="colour" style="display: inline-block;"><div class="colour-circle" style="background:' + productColours2[key] + '"></div><br><p class="colour-name">' + key + '</p></div>');
      };

      $("#right-div > #productPetite").attr("id", "productPetite2"); // change #productPetite to #productPetite2 so we can write to it.
      if (productPetite2 == true) {
        $("#productPetite2").html("petite");
      }
      else {
        $("#productPetite2").html("");
      };

      $("#right-div > #productAlsoAvailable").attr("id", "productAlsoAvailable2");
      if (productAlsoAvailable2.length > 0) {
        $("#productAlsoAvailable2").html(productAlsoAvailable2);
      }
      else {
        $("#productAlsoAvailable2").html("");
      };


    }
    else { // if 'separates' is not true (i.e. we only have one product)
      $("#right-div").remove(); // remove right div if it is there
      $("#left-div").css({"width": "100%", "display": "block", "float": "none", "box-shadow": "none"}); // make left div fill the whole of bottom slider
    };

// End separates section

    // Write product bottom text (e.g. Debenhams exclusivity) to the DOM.
    $("#productBottomText").html(productBottomText);

    // if productAlsoAvailable text is present for the product, display it. Otherwise write an empty div.
    if (productAlsoAvailable.length > 0) {
      $("#productAlsoAvailable").html(productAlsoAvailable);
    }
    else {
      $("#productAlsoAvailable").html("");
    };

    // Add a fix for long colour names spilling into each other. If longcolourname is true, then make the .colour div wider.

    if (productLongColourName == true) {
      $(".colour").css("width", "25%");
    }
    else {
      $(".colour").css("width", "12%");
    };


    // Write main images (main hero and three side thumbnails) to the DOM
    $("#main-hero").css({"background" : "url('" + productLargeImageFront + "')", "background-size" : "cover"});
    $("#side-image-1").css({"background" : "url('" + productSmallImageFront + "')", "background-size" : "cover"});
    $("#side-image-2").css({"background" : "url('" + productSmallImageBack + "')", "background-size" : "cover"});
    $("#side-image-3").css({"background" : "url('" + productSmallImageDetail + "')", "background-size" : "cover"});

    // Test if the product has 5 or 15 ways in which to be worn. If it does, then we need to create a slideshow when side images 2 and 3 are clicked.
    // If it doesn't, then we need to make sure that when side images 2 and 3 are clicked, they load just a single image in the #main-hero section.

    if (productIn5Or15 == true) {
      $("#side-image-2, #side-image-3").click(function(){ // when either side image 2 or 3 is clicked
        $("#main-hero").css("background", ""); // remove background image to make way for slider
        $("#slider").remove(); // remove any sliders that already exist
        $("#main-hero").prepend("<div id='slider' class='swipe'><div class='swipe-wrap'></div><span class='nav prev'></span><span class='nav next'></span></div>") // create slider div in #main-hero

        for (var key in productIn5Or15Images) { // Loop through the images that make up the slider for the 5 or 15 ways to wear this dress
          var sliderElement = "<div><img src='" + productIn5Or15Images[key] + "'width='448' height='655'/></div>"; // generate HTML for slider
          $(".swipe-wrap").append(sliderElement); // Append the HTML of slide elements to slide-wrap container
        };

        // initialise slider for 5 or 15 products, once DOM structure is in place as above
        Slider = $('#slider').Swipe({auto: 5000,speed: 1000,continuous: true}).data('Swipe');
        $('.next').on('click', Slider.next); // listener for click/tap on .next div on slider (chevron)
	      $('.prev').on('click', Slider.prev);

      }); // end side image 2 or 3 click function

    } // end if statement

    else {
      $("#side-image-1, #side-image-2, #side-image-3").click(function() { // when any one of the 3 side elements are clicked
       $("#slider").remove(); // remove slider if it is there
       var backgroundImage = $(this).css("background-image"); // grab the current URL of the background image for the element that is clicked (1,2 or 3)
       var newBackgroundImage = backgroundImage.replace("small", "large"); // modify this URL to use the LARGE folder instead
       $("#main-hero").css({"background" : newBackgroundImage, "background-size" : "cover"});  // write this to the background image value for the main image
      });
    };

    $("#bottom-slider").animate({marginBottom: "-421px"}, 1000); // when a thumbnail clicked, bring the bottom slider down to the bottom so it's ready to be clicked and slid up
    $("#ask-for-availability-container").css("display", "none");

  }); // end bottom thumbnail click function

// simulate click on the first thumbnail to load all the images
    $('#1').trigger('click');

// add in jQuery Mobile touch events for bottom slider

    $(function(){
      $("#bottom-slider").on("swipeup", swipeUp); // add swipeUp event listener

      function swipeUp(event) {
        $("#bottom-slider").animate({marginBottom: "0px"}, 1000); // set margin-bottom to 0 instead of -411px, so it's displayed full height and not hidden
        $("#ask-for-availability-container").css("display", "block"); // show product information
        $("#sizes").css("display", "block");
        $("#colours, #colours2").css("display", "block");
        $("#productPetite, #productPetite2").css("display", "block");
        $("#productAlsoAvailable, #productAlsoAvailable2").css("display", "block");
        console.log("swipe up");
      }

      $("#bottom-slider").on("swipedown", swipeDown); // add swipeDown event listener

      function swipeDown(event) {
        $("#bottom-slider").animate({marginBottom: "-541px"}, 1000);
        console.log("swipe down");
      }

    });

  // When the bottom slider is clicked, change hidden divs to display: block and slide it up

  $("#bottom-slider").click(function(){
    $("#ask-for-availability-container").css("display", "block"); // show product information
    $("#sizes").css("display", "block");
    $("#colours, #colours2").css("display", "block");
    $("#productPetite, #productPetite2").css("display", "block");
    $("#productAlsoAvailable, #productAlsoAvailable2").css("display", "block");
  });

    // Prevent vertical scrolling bounce on iPad - solution from http://gregsramblings.com/

    var xStart, yStart = 0;

    document.addEventListener('touchstart',function(e) {
        xStart = e.touches[0].screenX;
        yStart = e.touches[0].screenY;
    });

    document.addEventListener('touchmove',function(e) {
        var xMovement = Math.abs(e.touches[0].screenX - xStart);
        var yMovement = Math.abs(e.touches[0].screenY - yStart);
        if((yMovement * 3) > xMovement) {
            e.preventDefault();
        }
    });

    // add a script to fire a JavaScript alert when the app has cached successfully.

    window.applicationCache.addEventListener('cached', function() {
        alert("The Coast Bridesmaids Lookbook app has cached successfully. You can now use this offline.");
    }, false );

    // add an event listener to create a div that notifies the user that the app is downloading.

    window.applicationCache.addEventListener('progress', function(event) {
      $("#caching-progress").remove();
      $("body").prepend("<div id='caching-progress'></div>");
      $("#caching-progress").css({"position" : "absolute", "top" : "0", "background" : "green", "color" : "white", "text-align" : "center",
        "width" : "100%", "padding" : "20px 0px", "z-index" : "1", "font-size" : "24px"});

      $("#caching-progress").html("Downloading " + event.loaded + " items of " + event.total + " total. Please wait.");
    }, false);

    // remove this downloading div once the app has cached
    window.applicationCache.addEventListener('cached', function() {
        $("#caching-progress").remove();
    }, false );

}); // end script.