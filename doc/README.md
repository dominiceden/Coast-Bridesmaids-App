---------COAST BRIDESMAIDS LOOKBOOK V1.0-----------
--------Last updated on 18th February 2015---------

CONTENTS:

1/ DATABASE
2/ EMAIL ME WHEN IN STOCK

---------------------------------------------------------------------------

1/ DATABASE

The file products.js holds all the product information in JSON format. We iterate through this
to populate the DOM with product information.

The .manifest file holds the filenames of all the files that will be stored offline.
If you want to update the manifest, you can generate a list of all the files (including
directory paths) by using the following command in Terminal (Mac OS X).

find ~/Documents/Coast -type f

(where Documents/Coast is the folder containing all files).

*** ALL MANIFEST FILENAMES AND FOLDER NAMES MUST BE LOWER CASE. THE MANIFEST IS CASE SENSITIVE ***

---------------------------------------------------------------------------

2/ EMAIL ME WHEN IN STOCK

We use separate iframes (located in /iframes) for this feature. Each iframe contains a fragment of an
eDialog poll. The poll in its entirety (NOT the fragment) contains single fields for name, email address, date of birth,
date of wedding, a checkbox to sign up for the newsletter (unchecked by default with a value of NO) and a checkbox
for all the products that are not yet in stock.

Each iframe contains the single fields for name, email address, date of birth, date of wedding, the checkbox
to sign up for the newsletter and a hidden and pre-checked checkbox with a value of YES for the product in question. No other
products are shown. The user does not see the hidden checkbox when they view the iframe, so it just looks like
they are filling in a form and not selecting a dress - the dress is pre-selected for them.

When we export the poll from eDialog, we can then filter by which dresses have a value of YES to see all the details of users
who want to be informed when a particular dress is back in stock.

---------------------------------------------------------------------------



